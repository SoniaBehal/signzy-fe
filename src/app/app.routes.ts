import {Routes} from "@angular/router";
import {LoginComponent} from "../app/login/login.component";
import {SignupComponent} from "../app/signup/signup.component";
import {MusicSiteComponent} from "../app/music-site/music-site.component";
export const ROUTES=[
    {path:'',redirectTo:'login',pathMatch:'full'},
    {path:'login',component:LoginComponent},
    {path:'signup',component:SignupComponent},
    {path:'music',component:MusicSiteComponent}
]