import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicSiteComponent } from './music-site.component';

describe('MusicSiteComponent', () => {
  let component: MusicSiteComponent;
  let fixture: ComponentFixture<MusicSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
