import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user.service";
declare var soundManager;
@Component({
  selector: 'app-music-site',
  templateUrl: './music-site.component.html',
  styleUrls: ['./music-site.component.css']
})
export class MusicSiteComponent implements OnInit {
  playlist=[];
  constructor(public userService:UserService) { }

  ngOnInit() {
    console.log("current user  ",this.userService.currentUser)
    if(this.userService.currentUser)
    this.playlist=this.userService.currentUser.playlist;
  }

  playSound(link){
    let mySound = soundManager.createSound({
      url: link
  });
  mySound.play();
  }



}
