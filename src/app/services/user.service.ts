import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { environment } from "../../environments/environment";
import { Response, Headers, RequestOptions } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  headers: any;
  options: any;
  currentUser:any;
  constructor(private _http: Http
  ) {
    this.headers = new Headers({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    this.options = new RequestOptions({ headers: this.headers, method: 'post' })
  }
 
  login(data) {
    return this._http.post(environment.url + "login", data, this.options)
      .toPromise()
  }

  register(data) {
    return this._http.post(environment.url + "signup", data, this.options)
      .toPromise()
  }
}
