import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from "@angular/forms";
import {UserService} from "../services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm:FormGroup;
  errorResponse;
  constructor(private fb:FormBuilder,
    private router: Router,
    private userService:UserService) { }

  ngOnInit() {
    this.signupForm=this.fb.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmpassword:['',Validators.required]
    })
  }
  
 async register(){
    let user=await this.userService.register(this.signupForm.value);
    let mappeduser=user.json();
    if(mappeduser.error) this.errorResponse=mappeduser.error;
    else{
      this.userService.currentUser=mappeduser.user;
      this.router.navigate(["music"])
    }
    console.log("user   ",user)
  }
}
