import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder,FormGroup, Validators } from "@angular/forms";
import {UserService} from "../services/user.service";
declare var window:any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  errorMessage;
  constructor(private router: Router,
    private fb: FormBuilder,
    private userService:UserService) { }

  ngOnInit() {
    this.loginForm=this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  navigate() {
    this.router.navigate(["signup"])
  }

  async login() {
    console.log("login form   ",this.loginForm)
    let user=await this.userService.login(this.loginForm.value)
    let mappedUser=user.json();
    if(mappedUser.error)
    {
     this.errorMessage=mappedUser.error
    }
    else{
      this.userService.currentUser=mappedUser.user
      this.router.navigate(["music"])
    }
    console.log("user  ",mappedUser)
  }

}
