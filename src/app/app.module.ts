import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { MusicSiteComponent } from './music-site/music-site.component';
import {RouterModule} from "@angular/router"
import {ROUTES} from "./app.routes";
import {ReactiveFormsModule,FormsModule} from "@angular/forms";
import {UserService} from "../app/services/user.service";
import {HttpModule} from "@angular/http";
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    MusicSiteComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
